<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [Stream](./calckey-js.stream.md) &gt; [removeSharedConnection](./calckey-js.stream.removesharedconnection.md)

## Stream.removeSharedConnection() method

**Signature:**

```typescript
removeSharedConnection(connection: SharedConnection): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  connection | SharedConnection |  |

**Returns:**

void


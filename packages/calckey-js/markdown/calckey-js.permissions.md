<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [permissions](./calckey-js.permissions.md)

## permissions variable

**Signature:**

```typescript
permissions: string[]
```

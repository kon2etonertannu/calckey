<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [entities](./calckey-js.entities.md) &gt; [DetailedInstanceMetadata](./calckey-js.entities.detailedinstancemetadata.md)

## entities.DetailedInstanceMetadata type

**Signature:**

```typescript
export declare type DetailedInstanceMetadata = LiteInstanceMetadata & {
	features: Record<string, any>;
};
```
**References:** [LiteInstanceMetadata](./calckey-js.entities.liteinstancemetadata.md)

